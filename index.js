import axios from "axios";

if (process.argv.length < 3) {
  console.error("Пожалуйста, укажите название персонажей для поиска.");
  process.exit(1);
}

const nameHerois = process.argv.slice(2);

const getNameHeroi = async (parametrs) => {
  const result = await Promise.all(
    parametrs.map((nameHeori) =>
      axios
        .get(`https://swapi.dev/api/people/?search=${nameHeori}`)
        .then((responce) => responce.data)
        .catch((err) => {
          console.error(err);
          return { count: 0, results: [] };
        })
    )
  );
  let allResult = 0;
  let allName = [];
  let height = {
    nameMinHeight: "",
    nameMaxHeight: "",
    minHeight: Number.MAX_VALUE,
    maxHeight: Number.MIN_VALUE,
  };
  result.forEach((item, index) => {
    if (item.count === 0) {
      console.log(`No results found for '${parametrs[index]}'.`);
    } else {
      item.results.forEach((it) => {
        if (allName.includes(it.name)) {
          return;
        }
        allName.push(it.name);

        const heightValue = parseFloat(it.height);

        if (!isNaN(heightValue)) {
          if (heightValue < height.minHeight) {
            height.minHeight = heightValue;
            height.nameMinHeight = it.name;
          }
          if (heightValue > height.maxHeight) {
            height.maxHeight = heightValue;
            height.nameMaxHeight = it.name;
          }
        }
      });
    }
    allResult += item.count;
  });

  const allNameSort = allName.sort((a, b) => a.localeCompare(b)).join(", ");
  if (result.length === 0) {
    return;
  } else {
    console.log(`Total results: ${allResult}`);
    console.log(`All: ${allNameSort}`);
    console.log(`Min height: ${height.nameMinHeight}, ${height.minHeight}`);
    console.log(`Max height: ${height.nameMaxHeight}, ${height.maxHeight}`);
  }
  return result;
};

getNameHeroi(nameHerois);
