import axios from "axios";

async function searchStarWarsPeople(queries) {
  if (queries.length === 0) {
    console.log("Please provide at least one search query.");
    return;
  }

  const results = [];
  const errors = [];

  // Функция для выполнения запроса к SWAPI
  async function searchPeople(query) {
    try {
      const response = await axios.get(`https://swapi.dev/api/people/?search=${encodeURIComponent(query)}`);
      return response.data.results;
    } catch (error) {
      errors.push(error.message);
      return [];
    }
  }

  // Выполняем запросы параллельно
  const searchPromises = queries.map((query) => searchPeople(query));
  const searchResults = await Promise.all(searchPromises);

  // Обработка результатов поиска
  searchResults.forEach((result, index) => {
    if (result.length === 0) {
      console.log(`No results found for '${queries[index]}'`);
    } else {
      results.push(...result);
    }
  });

  // Вывод информации о персонажах
  if (results.length > 0) {
    console.log(`Total results: ${results.length}.`);
    const sortedNames = results.map((person) => person.name).sort();
    console.log(`All: ${sortedNames.join(", ")}.`);

    results.sort((a, b) => a.height - b.height);
    console.log(`Min height: ${results[0].name}, ${results[0].height} cm.`);
    console.log(`Max height: ${results[results.length - 1].name}, ${results[results.length - 1].height} cm.`);
  }

  // Вывод ошибок, если они были
  if (errors.length > 0) {
    console.log("Errors:");
    errors.forEach((error) => console.log(error));
  }
}

const queries = process.argv.slice(2);

searchStarWarsPeople(queries);
